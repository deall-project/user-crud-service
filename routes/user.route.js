"use strict"

const router = require('express').Router()
    , userController = require('../controllers/user.controller')
    , auth = require('../middleware/auth')


router.get('/user/info', auth.allUser, userController.getUserDetails)
router.get('/user/list', auth.admin, userController.getAllUser)
router.delete('/user/delete/:id', auth.admin, userController.deleteUser)
router.put('/user/update/level', auth.admin, userController.updateUserLevel)
router.put('/user/update/password', auth.allUser, userController.changePassword)
router.get('/user/:id', auth.admin, userController.getUserById)

module.exports = router