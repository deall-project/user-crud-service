"use strict"

const { Sequelize } = require('sequelize')
const db = require('../config/connection')

const User = db.define('user', {
    username: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    level: {
        type: Sequelize.ENUM('admin', 'user'),
        allowNull: false
    }
}, {
    freezeTableName: true,
    underscored: true
})

module.exports = User;