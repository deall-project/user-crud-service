FROM node:15.4.0-alpine3.10

# update packages
RUN apk update

# install redis

# create root application folder
WORKDIR /src/app

RUN npm install

# copy over all remaining source code to root application folder
COPY ./ ./

# check files list
RUN ls -a

EXPOSE 3001

CMD [ "node", "./app.js" ]
