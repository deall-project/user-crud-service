"use strict"

const { Sequelize, DataType } = require('sequelize')
const config = require('./index')
const db = new Sequelize(
    config.mysqlConfig.database,
    config.mysqlConfig.username,
    config.mysqlConfig.password,
    {
        host: config.mysqlConfig.host,
        dialect: 'mysql'
    }
)

// Connection test
db.authenticate()
.then(() => console.log('Mysql connected !'))
.catch(err => console.log('Connection failed: '+ err))

// sinkronisasi database hanya diaktifkan ketika running pertama kali atau ada perubahan dan tambahan di folder model, setelah itu dikomen lagi
db.sync({ force: false, alter: true}) 

module.exports = db;