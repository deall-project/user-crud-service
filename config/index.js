"use strict"

require('dotenv').config()

let config = {
    port: process.env.PORT || 3001,
    mysqlConfig: {
        host: process.env.DB_HOST,
        database: process.env.DB_NAME,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
    },
    token_secret: process.env.TOKEN_SECRET
}

module.exports = config
