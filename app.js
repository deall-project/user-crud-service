"use strict"

const express = require('express')
    , config = require('./config')
    , userRoute = require('./routes/user.route')
    , port = config.port
    , app = express()

app.use(express.json())

app.use('/api', userRoute)

app.get('/', (req, res) => {
    res.send('Wellcome to Deall Test API')

})
app.all('*', (req, res) => {
    res.send('Are you lost ?')
})

app.listen(port, () => {
    console.log(`Server running on port: ${port}`)
})