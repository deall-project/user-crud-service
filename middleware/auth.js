"use strict"

const jwtLib = require('../libraries/jwt.lib')

let middleware = {
    allUser: async (req, res, next) => {
        try {
            let token = req.headers.authorization
            if(!token) throw new Error('Unauthorized')

            let user = jwtLib.verify(token)
            if(!user) throw new Error('Unauthorized')

            req.user_id = user.id
            req.user = user

            next()
        } catch (error) {
            res.status(401).json({status: "failed", message: error.message})
        }
    },
    admin: async (req, res, next) => {
        try {
            let token = req.headers.authorization
            if(!token) throw new Error('Unauthorized')

            let user = jwtLib.verify(token)
            if(!user) throw new Error('Unauthorized')
            if(user.level != 'admin') throw new Error('Admin only!')

            req.user_id = user.id
            req.user = user

            next()
        } catch (error) {
            res.status(401).json({status: "failed", message: error.message})
        }
    }
}

module.exports = middleware;