"use strict"

const User = require('../models/user.model')
const bcryptLib = require('../libraries/bcrypt.lib')

let userController ={
    // related user
    getUserDetails: async (req, res) => {
        try {
            let id = req.user_id
            let result = await User.findOne({where: {id}})
            if(!result) throw new Error(`User dengan id: ${id} tidak ditemukan`)

            res.status(200).json({
                status: 'Success',
                data: result
            })
        } catch (error) {
            res.status(400).json({
                status: 'Failed',
                message: error.message
            })
        }
    },

    // admin only
    getUserById: async (req, res) => {
        try {
            let id = req.params.id
            let result = await User.findOne({where: {id}})
            if(!result) throw new Error(`User dengan id: ${id} tidak ditemukan`)

            res.status(200).json({
                status: 'Success',
                data: result
            })
        } catch (error) {
            res.status(400).json({
                status: 'Failed',
                message: error.message
            })
        }
    },

    // admin only
    getAllUser: async (req, res) => {
        try {
            let result = await User.findAll({attributes: {exclude: ["password"]}})
            if(!result) throw new Error('Data tidak ditemukan')

            res.status(200).json({
                status: 'Success',
                data: result
            })
        } catch (error) {
            res.status(400).json({
                status: 'Failed',
                message: error.message
            })
        }
    },

    // admin only
    updateUserLevel: async (req, res) => {
        try {
            let {username, level} = req.body
            if(!username || !level) throw new Error('Permintaan tidak lengkap')
            if(level != 'admin' && level != 'user') throw new Error('Format level salah')
            let user_exist = await User.findOne( {attributes: { exclude: ["password"]}}, {where: {username}})
            if(!user_exist) throw new Error(`User tidak ditemukan`)

            await User.update({level}, {where: {username}})
            res.status(200).json({ status: 'Success'})
        } catch (error) {
            res.status(401).json({
                status: 'Failed',
                message: error.message
            })
        }
    },

    // related user
    changePassword: async (req, res) => {
        try {
            let id = req.user_id
            let password = req.body.password
            if(password == '' || password == undefined || password == null || password.length < 6) throw new Error('Password tidak benar')
            password = bcryptLib.hasher(req.body.password)
            await User.update({password}, {where: {id}})
            res.status(200).json({ status: 'Success'})
        } catch (error) {
            res.status(401).json({
                status: 'Failed',
                message: error.message  
            })
        }
    },

    // admin only
    deleteUser: async (req, res) => {
        try {
            let id = req.params.id
            let user_exist = await User.findOne({ where: {id}})
            if(!user_exist) throw new Error('User tidak ditemukan')

            await User.destroy({ where: {id}})
            res.status(200).json({ status: 'Success'})
        } catch (error) {
            res.status(400).json({
                status: 'Failed',
                message: error.message
            })
        }
    }
}

module.exports = userController