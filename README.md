# Application Name: User Crud Service

# DEVELOPER: Muhammad Ary Widodo

# How to run

```bash
$ cd auth-service
$ npm i
$ npm start
```

# API List

TRAVELLER API

| Routes | EndPoint                            | Description                                            |
| ------ | ----------------------------------- | ------------------------------------------------------ |
| GET    | /api/user/info                      | Get user detail (Related user id)                      |
| GET    | /api/user/list                      | Get all users (Admin Only)                             |
| DELETE | /api/user/delete/:id                | Delete selected user by id (Admin only)                |
| PUT    | /api/user/update/level              | Update users level (Admin only)                        |
| PUT    | /api/user/update/password           | Update password (Related user)                         |
| GET    | /api/user/:id                       | Get user by ID (Admin only)                            |

```
GET
/api/user/info
id: Number

GET
/api/user/list

DELETE
/api/user/delete/:id
id: Number

PUT
/api/user/update/level
username: String
level: String

PUT
/api/user/update/password
id: Number
password: String

GET
/api/user/:id
id: Number