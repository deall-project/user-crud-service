"use strict"

const Joi = require('@hapi/joi')

let authSchema = Joi.object({
    username: Joi.string().min(3).required(),
    password: Joi.string().min(6).required()
})

module.exports = authSchema;